import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter layout demo'),
      ),
      body: Column(
        children: [
          //Image
          Expanded(
              flex: 4,
              child: Container(
                  child: Image.network(
                      "https://i.guim.co.uk/img/media/a6a3dfbaaff0b2a556a71467a015bddd6fde352c/0_952_1999_1199/master/1999.jpg?width=1200&quality=85&auto=format&fit=max&s=f5d5f2d5f6b3bdae08fd8662576085db"))),
          //Title
          Expanded(
            flex: 1,
            child: Row(
              children: [
                //Title
                Expanded(
                  flex: 4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Rennes Lake Campground',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w600),
                      ),
                      Text(
                        'Rennes, France',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Text('🎖', style: TextStyle(fontSize: 30)),
                      Text('41', style: TextStyle(fontSize: 20)),
                    ],
                  ),
                )
              ],
            ),
          ),
          //CTA
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    IconButton(
                        icon: Icon(Icons.phone, color: Colors.blue),
                        onPressed: () {}),
                    Text(
                      'CALL',
                      style: TextStyle(color: Colors.blue),
                    )
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                        icon: Icon(Icons.map, color: Colors.blue),
                        onPressed: () {}),
                    Text(
                      'ROUTE',
                      style: TextStyle(color: Colors.blue),
                    )
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                        icon: Icon(Icons.share, color: Colors.blue),
                        onPressed: () {}),
                    Text(
                      'SHARE',
                      style: TextStyle(color: Colors.blue),
                    )
                  ],
                ),
              ],
            ),
          ),
          //Description
          Expanded(
            flex: 6,
            child: Container(
              child: SingleChildScrollView(
                child: Text(
                    "Lforem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla suscipit ipsum non cursus. Sed suscipit maximus bibendum. Integer volutpat massa vel eros facilisis, feugiat dignissim lorem cursus. Integer non cursus libero. Vestibulum id elit sit amet tortor facilisis sodales. Praesent ornare vitae lorem ac pretium. Nam non turpis sagittis augue condimentum lobortis rhoncus ac nisl. Maecenas sodales faucibus urna nec facilisis. Etiam consequat tellus lectus, nec varius neque dignissim in. Nunc dapibus semper erat ut ultricies. Fusce quis finibus felis, eu gravida mi. Donec placerat, quam quis finibus placerat, leo nibh tincidunt nulla, sollicitudin sagittis lorem mi id augue. Nam vulputate gravida nisi eget porttitor. Pellentesque in dolor ultrices, congue sem sit amet, imperdiet lacus. Quisque at turpis enim. Donec eu congue leo.Nullam egestas viverra dui sed fringilla. Morbi pellentesque elementum est. Maecenas efficitur nunc aliquet ornare cursus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin velit purus, sodales nec ante ut, fringilla tincidunt ante. Aliquam sollicitudin sed nisl nec posuere. Aenean venenatis aliquet metus, non dapibus justo volutpat ac. Vestibulum pellentesque arcu et risus placerat, et congue leo consectetur. Pellentesque ac ipsum aliquam leo mollis placerat ultrices at felis. Morbi ante sem, pulvinar fringilla urna a, lacinia hendrerit purus. Aliquam sit amet velit arcu. Phasellus nunc eros, mollis elementum finibus sit amet, iaculis a ex. Morbi quam est, pulvinar at velit vel, tincidunt gravida lacus. Pellentesque venenatis nisl nisl, vitae ullamcorper nunc tincidunt vitae. Ut enim odio, laoreet sed tellus tempus, ultrices ultricies ex.Vestibulum ut ullamcorper libero, a semper velit. Phasellus eget velit sit amet ex malesuada condimentum. Integer vitae ex ac mi elementum congue. Maecenas sit amet enim enim. Suspendisse varius dui venenatis, vulputate turpis id, mattis leo. Integer eleifend erat vitae leo feugiat scelerisque. Nulla facilisi. Fusce porttitor vestibulum nisi sed eleifend. Donec ut aliquet sem, id cursus diam. Donec pulvinar viverra lectus vel blandit."),
              ),
              padding: EdgeInsets.all(25),
            ),
          ),
        ],
      ),
    );
  }
}
